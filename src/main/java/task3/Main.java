package task3;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        Main main=new Main();
        main.executorFirst();
        main.executorSecond();
        main.executorThird();
        main.executorFourth();
    }
    void executorFirst(){
        FibbonacciNumbers numbers=new FibbonacciNumbers(8);
        ExecutorService service= Executors.newSingleThreadExecutor();

        service.submit(numbers);

        service.shutdown();
    }
    void executorSecond(){
        FibbonacciNumbers numbers=new FibbonacciNumbers(8);
        ExecutorService service= Executors.newCachedThreadPool();;

        service.submit(numbers);

        service.shutdown();
    }
    void executorThird(){
        FibbonacciNumbers numbers=new FibbonacciNumbers(8);
        ExecutorService service= Executors.newCachedThreadPool();;

        service.submit(numbers);

        service.shutdown();
    }
    void executorFourth(){
        FibbonacciNumbers numbers=new FibbonacciNumbers(8);
        ExecutorService service= Executors.newCachedThreadPool();;

        service.submit(numbers);

        service.shutdown();
    }

}
