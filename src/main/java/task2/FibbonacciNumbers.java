package task2;

public class FibbonacciNumbers extends Thread {
    private int n;

    public FibbonacciNumbers(int n) {
        this.n = n;
    }

    @Override
    public void run() {
        getFibNumbers();
    }

    public void getFibNumbers(){
        int[] fib =new int[n];
        fib[0]=1;
        fib[1]=1;
        for (int i = 2; i <n ; i++) {
            fib[i]=fib[i-2]+fib[i-1];
        }
        for (int i = 0; i <n ; i++) {
            System.out.println(fib[i]);
        }
    }


}
