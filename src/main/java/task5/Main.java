package task5;

import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x=sc.nextInt();
        ScheduledExecutorService se = Executors.newScheduledThreadPool(5);
        long start = System.currentTimeMillis();

        for (int i = 0; i <x; i++) {
            se.schedule(new ThreadClass(start), ((int)(Math.random()*10)), TimeUnit.SECONDS);

        }
        se.shutdown();
    }
    static class ThreadClass implements Runnable{
        private long start;
        public ThreadClass(long start) {
            this.start=start;
        }
        @Override
        public void run() {
            System.out.println((new Date().getTime()- start + "ms."));
        }
    }
}
