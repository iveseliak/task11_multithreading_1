package task6;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {


        final FirstClass firstClass = new FirstClass(6,12);
        SecondClass secondClass = new SecondClass();
        ExecutorService service = Executors.newFixedThreadPool(3);

        service.submit(()->firstClass.methodAdd());
        service.submit(()->firstClass.methodDivision());
        service.submit(()->firstClass.methodMultiplication());


        service.submit(()->secondClass.methodAdd(16,32));
        service.submit(()->secondClass.methodDivision(16,32));
        service.submit(()->secondClass.methodMultiplication(16,32));

    }
}
