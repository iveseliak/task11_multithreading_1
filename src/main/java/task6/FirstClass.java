package task6;

public class FirstClass extends Thread{
    int a;
    int b;
    public FirstClass(int a, int b) {
        this.a=a;
        this.b=b;
    }

    public synchronized void methodAdd(){
        System.out.println(a+b);
    }

    public synchronized void methodMultiplication(){
        System.out.println(a*b);
    }

    public synchronized void methodDivision(){
        System.out.println((double)a/b);
    }
}
