package task6;

public class SecondClass extends Thread {

    private Object o1 = new Object();
    private Object o2 = new Object();
    private Object o3 = new Object();

    public void methodAdd(int a, int b) {
        synchronized (o1) {
            System.out.println(a + b);
        }
    }

    public void methodMultiplication(int a, int b) {
        synchronized (o2) {
            System.out.println(a * b);
        }
    }

    public void methodDivision(int a, int b) {
        synchronized (o3) {
            System.out.println((double) a / b);
        }
    }
}
