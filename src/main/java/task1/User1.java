package task1;

public class User1 extends Thread{

    @Override
    public void run() {
        synchronized (this) {
            System.out.println("ping");
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            notify();
        }
    }
}
