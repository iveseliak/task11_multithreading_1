package test;

public class WaitNotifyApp {

    public static void main(String[] args) throws InterruptedException {
        ThreadB threadB=new ThreadB();
        threadB.start();
        synchronized (threadB){
            threadB.wait();
        }
        System.out.println(threadB.total);
    }
    static class ThreadB extends Thread{
        int total=0;

        @Override
        public void run() {
            synchronized (this){
            for (int i = 0; i <5 ; i++) {
                total+=i;
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            notify();
        }
        }
    }
}
