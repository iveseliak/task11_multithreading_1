package test;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class SyncronizeCollectionApp {
    public static void main(String[] args) {
        final NameList nameList=new NameList();
        nameList.add("first");
        class MyThread extends Thread{
            @Override
            public void run() {
                System.out.println(nameList.removeFirst());
            }
        }
        MyThread myThread5=new MyThread();
        myThread5.setName("one");
        myThread5.start();
        new MyThread().start();
    }

    static class NameList{
        private ArrayList list= new ArrayList<>();
        public synchronized void add(String name){
            list.add(name);
        }
        public synchronized String removeFirst(){
            if (list.size()>0){
                if (Thread.currentThread().getName().equals("one")){
                    Thread.yield();
                }
                return (String) list.remove(0);
            }
            return null;
        }
    }
}
