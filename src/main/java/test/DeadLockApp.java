package test;

public class DeadLockApp {
    public static void main(String[] args) {
        ResourceA resourceA=new ResourceA();
        ResourceB resourceB=new ResourceB();
        resourceA.resourceB=resourceB;
        resourceB.resourceA=resourceA;
        MyThread5 myThread5=new MyThread5();
        MyThread6 myThread6=new MyThread6();
        myThread5.resourceA=resourceA;
        myThread6.resourceB=resourceB;
        myThread5.start();
        myThread6.start();
    }
}

class MyThread5 extends Thread{
    ResourceA resourceA;

    @Override
    public void run() {
        System.out.println(resourceA.getI());
    }
}

class MyThread6 extends Thread{
    ResourceB resourceB;

    @Override
    public void run() {
        System.out.println(resourceB.getI());
    }
}

class ResourceA{
    ResourceB resourceB;

    public synchronized int getI(){
        return resourceB.returnI();
    }
    public synchronized int returnI(){
        return 1;
    }
}

class ResourceB{
    ResourceA resourceA;

    public synchronized int getI(){
        return resourceA.returnI();
    }
    public synchronized int returnI(){
        return 1;
    }
}
