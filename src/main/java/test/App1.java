package test;

public class App1 {
    public static void main(String[] args) throws InterruptedException {

        Resource resource = new Resource();
        resource.i = 5;
        MyThread1 myThread = new MyThread1();
        MyThread1 myThread1 = new MyThread1();
        myThread.setResource(resource);
        myThread1.setResource(resource);
        myThread.start();
        myThread1.start();
        myThread.join();
        myThread1.join();
        System.out.println(resource.i);
    }
}

class MyThread1 extends Thread {
    Resource resource;

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    @Override
    public void run() {
        resource.changeI();
    }
}

class Resource {
    int i;

    public synchronized void changeI() {
        int i = this.i;
        i++;
        this.i = i;
    }
}
