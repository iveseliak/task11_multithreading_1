package test;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicApp {
    static AtomicInteger atomicInteger=new AtomicInteger(0);
    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i <10000 ; i++) {
            new MyThread3().start();
        }
        Thread.sleep(2000);
        System.out.println(atomicInteger);
    }
    static class MyThread3 extends Thread{
        @Override
        public void run(){
            atomicInteger.incrementAndGet();
        }
    }
}
