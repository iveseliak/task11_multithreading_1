package test;

public class Main {
    public static void main(String[] args) throws InterruptedException {
    new MyThread().start();
//    Thread.yield();
    new MyThread().join();
//    Thread.sleep(3000);
    new MyThread().start();
    }
}

class MyThread extends Thread{
    @Override
    public void run(){
        int a=0;
        for (int i = 0; i <100 ; i++) {
            System.out.println("this is new thread "+Thread.currentThread().getName()+" i ="+i);
            a++;
        }
        System.out.println(a);

    }

}
